import React, {Component} from 'react';
import axios  from '../../axios'
import {Jumbotron} from "reactstrap";

class Pages extends Component {
    state = {
      pages: null
    };

    loadedPage = () => {
        const name = this.props.match.params.name;
        axios.get('pages/' + name + '.json').then(respons => {
            this.setState({pages: respons.data})
        })
    };

    componentDidMount() {
         this.loadedPage()
    }

    componentDidUpdate(prevProps) {
        // console.log(this.props);
        if(this.props.match.params.name && this.props.match.params.name !== prevProps.match.params.name) {
            this.loadedPage()
        }
    }

    render() {
        if(!this.state.pages) return null;
        return (
            <div>
                <Jumbotron>
                    <h1 className="display-3">{this.state.pages.title}</h1>
                    <p className="lead">{this.state.pages.content}</p>

                </Jumbotron>
            </div>
        );
    }
}

export default Pages
