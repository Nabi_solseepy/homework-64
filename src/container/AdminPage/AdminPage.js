import React, {Component} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import axios from '../../axios'
class AdminPage extends Component {
    state = {
      content: '',
      title: '',
      pages : 'about'
    };

 ChangeValue = event => {
     const id = event.target.id;

     this.setState({[id]: event.target.value})
 };

 componentDidMount() {
     axios.get(`pages/${this.state.pages}.json`).then(respons => {
         this.setState({
             title: respons.data.title,
             content: respons.data.content
         })
     })
 };

 componentDidUpdate(prevProps, prevState) {
     if (this.state.pages !== prevState.pages) {
         axios.get(`pages/${this.state.pages}.json`).then(respons => {
             this.setState({
                 title: respons.data.title,
                 content: respons.data.content
             })
         })
     }

 }

 pageEdit = (event) => {
event.preventDefault()
     const data = {
        title: this.state.title,
         content: this.state.content
     };
     axios.put('pages/' + this.state.pages + '.json', data).then(() => {
         this.props.history.replace('/pages/' + this.state.pages)
     })
 };


    render(){
        return (
            <div>
                <div>
                    <Form className="ProductForm" mrt={10} onSubmit={(event) => this.pageEdit(event)}>
                        <h1>Select page</h1>
                        <FormGroup row>
                            <Label for="exampleEmail" sm={2}>Category</Label>
                            <Col sm={10}>
                                <Input type="select" name="pages" id="pages"
                                       value={this.state.pages} onChange={this.ChangeValue}
                                >
                                    ))}
                                    <option>about</option>
                                    <option>contacts</option>
                                    <option>divisions</option>
                                </Input>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="exampleEmail" sm={2}>Title</Label>
                            <Col sm={10}>
                                <Input type="text" name="name" id="title"
                                       value={this.state.title} onChange={this.ChangeValue}
                                />
                            </Col>
                        </FormGroup>


                        <FormGroup row>
                            <Label for="exampleEmail" sm={2}>Content</Label>
                            <Col sm={10}>
                                <Input type="textarea" name="description"
                                       id="content"
                                       value={this.state.content} onChange={this.ChangeValue}

                                />
                            </Col>
                        </FormGroup>
                        <FormGroup>
                            <Col sm={{size: 10, offset: 2}}>
                                <Button type="submit">Save</Button>
                            </Col>
                        </FormGroup>
                    </Form>
                </div>
            </div>
        );
    }
}

export default AdminPage;