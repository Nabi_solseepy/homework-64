import React from 'react';
import {Collapse, Nav, } from "reactstrap";

const NavigationNav = ({children}) => {
    return (
        <Collapse  navbar>
            <Nav className="ml-auto" navbar>
                {children}
            </Nav>
        </Collapse>
    );
};

export default NavigationNav;