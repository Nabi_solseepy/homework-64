import React, {Fragment} from 'react';
import {NavItem, NavLink} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";


const NavigationItem = () => {
    return (
         <Fragment>
             <NavItem>
                 <NavLink tag={RouterNavLink} to="/">Home</NavLink>
             </NavItem>
             <NavItem>
                 <NavLink tag={RouterNavLink} to="/pages/about">About</NavLink>
             </NavItem>
             <NavItem>
                 <NavLink  tag={RouterNavLink} to="/pages/contacts">Contacts</NavLink>
             </NavItem>
             <NavItem>
                 <NavLink tag={RouterNavLink} to="/pages/divisions">Divisons</NavLink>
             </NavItem>
             <NavItem>
                 <NavLink tag={RouterNavLink} to="/pages/admin" >Admin</NavLink>
             </NavItem>
         </Fragment>
    );
};

export default NavigationItem;