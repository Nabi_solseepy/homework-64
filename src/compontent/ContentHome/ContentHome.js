import React from 'react';
import {Jumbotron} from "reactstrap";

const ContentHome = () => {
    return (
        <div>
            <Jumbotron>
                <h1 className="display-3">Home</h1>
                <p className="lead"> Title Home</p>

            </Jumbotron>
        </div>
    );
};

export default ContentHome;