import React from 'react';
import { Navbar} from "reactstrap";
import Logo from "../Logo/Logo";
import NavigationNav from "../Navigation/NavigationNav/NavigationNav";
import NavigationItem from "../Navigation/NavigationItem/NavigationItem";


const Header = () => {
        return (

            <Navbar color="dark"  dark expand="md">
                <Logo/>
                <NavigationNav>
                    <NavigationItem/>
                </NavigationNav>
            </Navbar>

        );
    };

export default Header;