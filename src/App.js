import React, {Component, Fragment} from 'react';
import './App.css';
import {BrowserRouter, Route} from "react-router-dom";
import Layout from "./compontent/Layout/Layout";
import '../node_modules/react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import Pages from "./container/Pages/Pages";
import AdminPage from "./container/AdminPage/AdminPage";
import ContentHome from "./compontent/ContentHome/ContentHome";



class App extends Component {
  render() {
    return (
        <BrowserRouter>
                     <Fragment>
                         <Layout>
                             <Route path="/"  exact component={ContentHome}/>
                             <Route path="/pages/admin"  component={AdminPage}/>
                             <Route path="/pages/:name"  component={Pages}/>

                         </Layout>
                     </Fragment>
        </BrowserRouter>
    );
  }
}

export default App;
